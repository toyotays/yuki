<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=shift-jis" />
<meta name="keywords" content="西宮市,鍼灸院,美容鍼灸,エステ" />
<meta name="description" content="兵庫県西宮市、JR「甲子園口」駅より徒歩2〜3分のところにある日曜日も開いている鍼灸院です。痩身 治療（ダイエット）、美顔鍼・エステには実績があります。" />
<title>幸鍼灸院（ゆきしんきゅういん）エステ・西宮市、ＪＲ「甲子園口」駅より徒歩２分〜３分</title>
<link href="css/import.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script><!--//back-to-->
<script type="text/javascript" src="js/multihero.js"></script><!--//back-to-->
<script type="text/javascript"> 
// pagetop
$(document).ready(function(){
	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 300);
			return false;
		});
	});

});
</script>

</head>


<body>
	
<div id="header">
    <div class="f-l">
        <h1>西宮のアッとホームな幸鍼灸院</h1>
        <h2><a href="index.php"><img src="img/common/logo.png" alt="ゆきしんきゅういん" /></a></h2>
    </div>
    <div style="float:left; padding-top: 20px;padding-left: 90px;">
        <a class="open_help_link" id="open_help_header" href="javascript:void(0)"><img src="img/common/highqualitymin01.jpg" alt="24時間web予約" width="250" height="67"/></a>
    </div>
    <ul class="f-r">
        <li><a href="inquiry.php"><img src="img/common/inquiry.png" alt="ご予約・お問い合わせ" /></a></li>
        <li><img src="img/common/tel.png" alt="TEL：0798-66-8834" /></li>
    </ul>
    <p id="back-top"><a href="#top"><span></span></a></p>
</div>

<ul id="navi">
    <li><a href="index.php"><img src="img/common/navi01.png" alt="HOME" /></a></li>
    <li><a href="beginner.php"><img src="img/common/navi02.png" alt="初めての方へ" /></a></li>
    <li><a href="menu.php"><img src="img/common/navi03.png" alt="料金" /></a></li>
    <li><a href="access.php"><img src="img/common/navi04.png" alt="診察時間・アクセス" /></a></li>
    <li><a href="director.php"><img src="img/common/navi05.png" alt="院長紹介" /></a></li>
    <li><a href="inquiry.php"><img src="img/common/navi06.png" alt="お問い合わせ" /></a></li>
    <li><a href="http://ameblo.jp/yuki-shinkyu/" target="_blank"><img src="img/common/navi07.png" alt="ブログ" /></a></li>
	<div style="clear:both"></div>
</ul>
  

<div id="container">
<div id="box_l">
<p class="sp-mb50"><a href="beginner.php"><img src="img/common/forbeginner.png" alt="はじめての方へ" width="210" height="210" /></a></p>
<h3><img src="img/common/smenu.png" alt="メニュー" /></h3>
<ul id="smenu">
<li><a href="menu1.php"><img src="img/common/sindiva.png" alt="INDIVA" /></a></li>
<li><a href="menu2.php"><img src="img/common/shari.png" alt="鍼灸" /></a></li>
<li><a href="menu3.php"><img src="img/common/sbiyo.png" alt="美容鍼灸" /></a></li>
<li><a href="menu4.php"><img src="img/common/sseitai.png" alt="美容整骨" /></a></li>
</ul>

<p class="sp-mt40"><img src="img/common/credit.png" alt="クレジットカード使えます" /></a></p>
<p class="sp-mt20"><a href="http://ameblo.jp/yuki-shinkyu/"><img src="img/common/sblog.png" alt="ブログ" /></a></p>
<p class="sp-mt20"><a href="director.php"><img src="img/common/sintyou.png" alt="院長紹介" /></a></p>

<h4 class="sp-mt40"><img src="img/common/slogo.png" alt="ゆきしんきゅういん" /></h4>
<div class="slogo">
<p>兵庫県西宮市甲子園口北町4-29</p>
<p>マンション三和102号</p>
<p>［ TEL ]　0798-66-8834</p>
</div>
<p><a href="inquiry.php"><img src="img/common/smail.png" alt="ご予約・お問い合わせ" /></a></p>
<p style="margin-top:15px;"><a class="open_help_link" id="open_help_header2" href="javascript:void(0)"><img src="img/common/highquality01.jpg" alt="24時間web予約" width="200" height="111" /></a></p>

<dl class="LinksSide">
	<dt><img src="img/common/link_ttl.png" alt="リンク" /></dt>
    <dd>
    	<ul>
        	<li><a href="http://www.yuki-shinkyu.com/cocolo/index.php" target="_blank"><img src="img/common/link_bnr01.png" alt="cocolo"/></a></li>
    	</ul>
    </dd>
</dl>

</div>

<div id="box_r">
<h3><img src="imges/menu.png" alt="治療メニュー" /></h3>
<ul id="menu_u1" class="sp-pb30">
<li><img src="imges/menu11.png" alt="初診料１０００円" width="251" height="37" /></li>
<li><img src="imges/menu2.png" alt="各種保険適応、ご相談ください" width="251" height="37" /></li>
</ul>
<div class="cle"></div>





<div id="menu_tb">
<table width="770" border="0">
  <tr>
    <td width="301" class="menu01">全身治療<span class="menu_indiva">インディバ込 </span><span class="menu_time">40〜60分</span></td>
    <td width="385" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">5,200円</td>
  </tr>
  </table>
  	<p class="Comment">お悩みの箇所や症状を伺いはり・きゅう・インデイバを使用しトータルケアしていきます。<br />
    【主な症状】　腰痛（ギックリ腰）、関節痛（膝、肩）、神経痛（肋間、坐骨、頚腕）など<br />
    その他、肩こり症、顔面麻痺・生理不順・眼精疲労など </p>
    
  
  <table width="770" border="0">
    <tr>
    <td width="300" class="menu01">局所治療<span class="menu_indiva">インディバ込 </span><span class="menu_time">20〜30分</span></td>
    <td width="386" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">4,100円</td>
  </tr>
  </table>
  	<p class="Comment">お悩みの箇所のみをツボをはり・きゅう・インデイバの全てを使用し治療を行います。<br />
    【主な症状】　腱鞘炎、捻挫、バネ指、リウマチ（関節痛）など</p>
    
  
  <table width="770" border="0">
    <tr>
    <td width="337" class="menu01">全身治療 ＋インディバ肩こりマッサージ<span class="menu_time">60〜80分</span></td>
    <td width="349" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">6,700円</td>
  </tr>
  </table>
  	<p class="Comment">全身治療（トータルケア）の施術の後にインディバマッサージ（15分）を行います。<br />
    肩こりが特にキツい方にオススメです。</p>
    
  
  <table width="770" border="0">
    <tr>
    <td width="298" class="menu01">美顔はり＋インディバ肩こりマッサージ<span class="menu_time">50分</span></td>
    <td width="388" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">7,500円</td>
  </tr>
  </table>
  	<p class="Comment">美容はりの施術前後にインディバマッサージ（20分）を行い美容効果をアップさせます。</p>
    
  
  <table width="770" border="0">
    <tr>
    <td width="244" class="menu01">インディバ肩こりマッサージ<span class="menu_time">20分</span></td>
    <td class="menu02">&nbsp;</td>
    <td width="70" class="menu03">3,800円</td>
  </tr>
  </table>
  	<p class="Comment">まず身体の表面を温め、それから深部に熱を入れながら頚部から肩までのコリをほぐしていきます。<br />
  	  普通のマッサージと比べ、インディバで熱を発しながらマッサージをいたしますので効果と気持ちよさは間違いなし！！ </p>

  
  <table width="770" border="0">
    <tr>
    <td width="214" class="menu01">鍼灸治療のみ<span class="menu_time">３０分 </span></td>
    <td width="470" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">3,900円</td>
  </tr>
  </table>
  
  
  <table width="770" border="0">
    <tr>
    <td width="232" class="menu01">全身＋美顔はり<span class="menu_time">50分 </span></td>
    <td width="454" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">6,500円</td>
  </tr>
  </table>
  	<p class="Comment">患者様の気になるお悩みを伺い痛みの少ない針で施術いたします。</p>
  
  
  <table width="770" border="0">
    <tr>
    <td width="309" class="menu01">全身＋美顔はり＋セルキュア（美顔器）<span class="menu_time">80分 </span></td>
    <td width="377" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">8,500円</td>
  </tr>
  </table>
  	<p class="Comment">患者様の気になるお悩みを伺い痛みの少ない針で施術いたします。<br />
    その後にセルキュア（美顔器）で更にフェイスラインを引きしめ、毛穴の開き、くすみ、ほうれいせん、たるみを改善していきます。</p>
  
  
  <table width="770" border="0">
    <tr>
    <td width="430" class="menu01">インディバ肩こりマッサージ＋美顔はり＋セルキュア（美顔器）<span class="menu_time">80分 </span></td>
    <td width="256" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">8,800円</td>
  </tr>
  </table>
  
  
  <table width="770" border="0">
    <tr>
    <td width="301" class="menu01">鍼灸治療＋耳ツボ＋インディバスリム<span class="menu_time">70分 </span></td>
    <td width="385" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">8,200円</td>
  </tr>
  </table>
  	<p class="Comment">はりきゅう治療で身体のこりや浮腫みの改善を目的とし耳つぼで食事のコントロール。<br />
    そしてインディバ施術（45分）で深部に熱を入れながら表面を温熱リンパマッサージしダイエットのサポートをいたします。</p>
  
 
  <table width="770" border="0">
    <tr>
    <td width="214" class="menu01">大学生<span class="menu_indiva">インディバ込 </span></td>
    <td width="470" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">3,300円</td>
  </tr>
  </table>
  <table width="770" border="0">
    <tr>
    <td width="214" class="menu01">高校生<span class="menu_indiva">インディバ込 </span></td>
    <td width="470" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">2,800円</td>
  </tr>
  </table>
  <table width="770" border="0">
    <tr>
    <td width="214" class="menu01">小中学生<span class="menu_indiva">インディバ込 </span></td>
    <td width="470" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">2,300円</td>
  </tr>
  </table>
  	<p class="Comment">主にスポーツ障害がメインですが肩こりや頭痛、喘息にも対応しております。</p>
  
  
  
  <table width="770" border="0">
    <tr>
    <td width="214" class="menu01">逆子治療<span class="menu_time">10分 </span></td>
    <td width="470" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">3,600円</td>
  </tr>
  </table>
  	<p class="Comment">治療内容はお灸がメインですが状態により針を使用することもあります。</p>
    
    
    
  </table>
  <table width="770" border="0">
    <tr>
    <td width="294" class="menu01">不妊治療<span class="menu_indiva">インディバ併用 </span><span class="menu_time">60分 </span></td>
    <td width="391" class="menu02">&nbsp;</td>
    <td width="71" class="menu03">6,400円</td>
  </tr>
  </table>
  	<p class="Comment">はりきゅうで全身の施術をし、インディバで深部に熱を入れていきます。</p>
  
  
  <table width="770" border="0">
    <tr>
    <td width="294" class="menu01">不妊治療＋全身治療<span class="menu_time">90分 </span></td>
    <td width="393" class="menu02">&nbsp;</td>
    <td width="69" class="menu03">8,400円</td>
  </tr>
  </table>
  	<p class="Comment">肩こりや腰痛、トータルケアと同時に不妊治療をいたします。</p>
    
    
   
<table width="770" border="0">
    <tr>
        <td width="88" class="menu01">美容整骨</td>
        <td width="113">〔　お試し価格　〕</td>
        <td width="126">小顔美容整骨</td>
        <td width="78"><span class="menu_time">３０分 </span></td>
        <td width="264" class="menu02">&nbsp;</td>
        <td width="71" class="menu03">6,500円</td>
  	</tr>
    <tr>
        <td width="88" >　</td>
        <td width="113">　</td>
        <td width="126">ボディ美容整骨</td>
        <td width="78"><span class="menu_time">４０分 </span></td>
      	<td width="264" class="menu02">　</td>
      	<td width="71" class="menu03">6,200円</td>
    </tr>
    <tr>
     	<td width="88" >　</td>
        <td>&nbsp;</td>
        <td>小顔全身美容整骨</td>
        <td width="78"><span class="menu_time">７０分 </span></td>
      	<td class="menu02">&nbsp;</td>
      	<td width="71" class="menu03">12,000円</td>
    </tr>
  </table>
  
  <table width="770" border="0">
    <tr>
    <td width="116"></td>
    <td width="110">〔　通常価格　〕</td>
    <td width="119">小顔美容整骨</td>
    <td width="84"><span class="menu_time">３０分 </span></td>
    <td width="244" class="menu02">&nbsp;</td>
    <td width="71" class="menu03">13,200円</td>
  </tr>
    <tr>
      <td width="116" >&nbsp;</td>
        <td></td>
        <td>ボディ美容整骨</td>
        <td width="84"><span class="menu_time">４０分 </span></td>
      <td class="menu02">&nbsp;</td>
      <td width="71" class="menu03">10,800円</td>
    </tr>
    <tr>
      <td width="116" >&nbsp;</td>
        <td></td>
        <td>小顔全身美容整骨</td>
        <td width="84"><span class="menu_time">７０分 </span></td>
      <td class="menu02">&nbsp;</td>
      <td width="71" class="menu03">22,400円</td>
    </tr>
  </table>
  <table width="770" border="0">
    <tr>
    <td width="115"></td>
    <td width="109">〔　<span lang="EN-US" xml:lang="EN-US">&nbsp;</span>チケット　〕５回</td>
    <td width="118">小顔美容整骨</td>
    <td width="83"><span class="menu_time">３０分 </span></td>
    <td width="231" class="menu02">&nbsp;</td>
    <td width="88" class="menu03">59,400円</td>
  </tr>
    <tr>
      <td width="115" >&nbsp;</td>
        <td></td>
        <td>ボディ美容整骨</td>
        <td width="83"><span class="menu_time">４０分 </span></td>
      <td class="menu02">&nbsp;</td>
      <td width="88" class="menu03">48,600円</td>
    </tr>
    <tr>
      <td width="115" >&nbsp;</td>
        <td></td>
        <td>小顔全身美容整骨</td>
        <td width="83"><span class="menu_time">７０分 </span></td>
      <td class="menu02">&nbsp;</td>
      <td width="88" class="menu03">100,800円</td>
    </tr>
  </table>
  <table width="770" border="0">
    <tr>
    <td width="114"></td>
    <td width="109">　　　　　　　　１０回</td>
    <td width="118">小顔美容整骨</td>
    <td width="83"><span class="menu_time">３０分 </span></td>
    <td width="233" class="menu02">&nbsp;</td>
    <td width="87" class="menu03">105,600円</td>
  </tr>
    <tr>
      <td width="114" >&nbsp;</td>
        <td></td>
        <td>ボディ美容整骨</td>
        <td width="83"><span class="menu_time">４０分 </span></td>
      <td class="menu02">&nbsp;</td>
      <td width="87" class="menu03">86,400円</td>
    </tr>
    <tr>
      <td width="114" >&nbsp;</td>
        <td></td>
        <td>小顔全身美容整骨</td>
        <td width="83"><span class="menu_time">７０分 </span></td>
      <td class="menu02">&nbsp;</td>
      <td width="87" class="menu03">179,200円</td>
    </tr>
  </table>
  
    
</div>  

<div class="sp-mt80">
<ul class="me_ft">
<li><a href="menu1.php"><img src="imges/me1.png" alt="インディバについて" /></a></li>
<li><a href="menu2.php"><img src="imges/me2.png" alt="鍼灸について" /></a></li>
<li><a href="menu3.php"><img src="imges/me3.png" alt="美容鍼灸について" /></a></li>
</ul>
<div class="cle"></div>
</div>

<div id="menu_endf">
<h4 class="sp-mb20">全身治療</h4>
<p class="sp-pb30">本来の東洋医学とは全身（心と身体）全てを診て何故その症状が発生するのか患者様自信の体質にアプローチし、
「肩コリ、腰痛、冷え性、生理痛」などの全ての症状に対して首、肩、腕、脚、お腹、背中など全身に鍼灸治療を施術することです。</p>
<h4 class="sp-mb20">局所治療</h4>
<p>身体の一部分のみを診て治療することです。<br />
　例）　肩コリで肩だけを治療する　等</p>
</div>


<div class="sp-mt50">
<p class="t-c"><img src="img/biginner/ftlo.png" width="191" height="41" /></p></div>
<ul id="ft1" class="sp-pb20">
    <li><a href="menu1.php"><img src="img/biginner/ft1.png" /></a></li>
    <li><a href="menu2.php"><img src="img/biginner/ft2.png" /></a></li>
    <li><a href="menu3.php"><img src="img/biginner/ft3.png" /></a></li>
</ul>
<ul id="ft1" style="padding:50px 0 0 0;">
    <li><a href="menu4.php"><img src="img/biginner/ft4.png" /></a></li>
</ul>
<div class="cle"></div>
<div id="fts_box">
<ul id="fts">
<li><a href="beginner.php"><img src="img/biginner/fts1.png" /></a></li>
<li><a href="menu.php"><img src="img/biginner/fts2.png" /></a></li>
<li><a href="access.php"><img src="img/biginner/fts3.png" /></a></li>
</ul>
</div>
<div class="cle"></div>

<div id="ft_end">
<div id="ft_end_l">
<h4 class="sp-pl-40"><img src="img/biginner/ff1.png" alt="駐車場のご案内" /></h4>
<div class="sp-pl_t30">
<p>駐車スペースの台数に限りがありますので、<br />
お車でお越しの方はお電話にてご確認下さい。</p>
</div>
</div>


<div id="ft_end_r">
<h4><img src="img/biginner/ff2.png" alt="ご予約・お問い合わせ" /></h4>
<div class="sp-mt20">
<img src="img/biginner/tel.png" alt="TEL0798-66-8834" />
 　　　<a href="inquiry.php"><img src="img/biginner/ftmail.png" width="100" height="28" /></a></div>
</div>
<div class="cle"></div>
</div>

</div><!-- box_r -->




</div><!-- container -->



<?php include("footer.html"); ?>

</body>
</html>
