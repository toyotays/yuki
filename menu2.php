<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=shift-jis" />
<meta name="keywords" content="西宮市,鍼灸院,美容鍼灸,エステ" />
<meta name="description" content="兵庫県西宮市、JR「甲子園口」駅より徒歩2〜3分のところにある日曜日も開いている鍼灸院です。痩身 治療（ダイエット）、美顔鍼・エステには実績があります。" />
<title>幸鍼灸院（ゆきしんきゅういん）エステ・西宮市、ＪＲ「甲子園口」駅より徒歩２分〜３分</title>
<link href="css/import.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script><!--//back-to-->
<script type="text/javascript" src="js/multihero.js"></script><!--//back-to-->
<script type="text/javascript"> 
// pagetop
$(document).ready(function(){
	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 300);
			return false;
		});
	});

});
</script>

</head>


<body>
	
<div id="header">
    <div class="f-l">
        <h1>西宮のアッとホームな幸鍼灸院</h1>
        <h2><a href="index.php"><img src="img/common/logo.png" alt="ゆきしんきゅういん" /></a></h2>
    </div>
    <div style="float:left; padding-top: 20px;padding-left: 90px;">
        <a class="open_help_link" id="open_help_header" href="javascript:void(0)"><img src="img/common/highqualitymin01.jpg" alt="24時間web予約" width="250" height="67"/></a>
    </div>
    <ul class="f-r">
        <li><a href="inquiry.php"><img src="img/common/inquiry.png" alt="ご予約・お問い合わせ" /></a></li>
        <li><img src="img/common/tel.png" alt="TEL：0798-66-8834" /></li>
    </ul>
    <p id="back-top"><a href="#top"><span></span></a></p>
</div>

<ul id="navi">
    <li><a href="index.php"><img src="img/common/navi01.png" alt="HOME" /></a></li>
    <li><a href="beginner.php"><img src="img/common/navi02.png" alt="初めての方へ" /></a></li>
    <li><a href="menu.php"><img src="img/common/navi03.png" alt="料金" /></a></li>
    <li><a href="access.php"><img src="img/common/navi04.png" alt="診察時間・アクセス" /></a></li>
    <li><a href="director.php"><img src="img/common/navi05.png" alt="院長紹介" /></a></li>
    <li><a href="inquiry.php"><img src="img/common/navi06.png" alt="お問い合わせ" /></a></li>
    <li><a href="http://ameblo.jp/yuki-shinkyu/" target="_blank"><img src="img/common/navi07.png" alt="ブログ" /></a></li>
	<div style="clear:both"></div>
</ul>
  

<div id="container">
<div id="box_l">
<p class="sp-mb50"><a href="beginner.php"><img src="img/common/forbeginner.png" alt="はじめての方へ" width="210" height="210" /></a></p>
<h3><img src="img/common/smenu.png" alt="メニュー" /></h3>
<ul id="smenu">
<li><a href="menu1.php"><img src="img/common/sindiva.png" alt="INDIVA" /></a></li>
<li><a href="menu2.php"><img src="img/common/shari.png" alt="鍼灸" /></a></li>
<li><a href="menu3.php"><img src="img/common/sbiyo.png" alt="美容鍼灸" /></a></li>
<li><a href="menu4.php"><img src="img/common/sseitai.png" alt="美容整骨" /></a></li>
</ul>
<p class="sp-mt40"><img src="img/common/credit.png" alt="クレジットカード使えます" /></a></p>
<p class="sp-mt20"><a href="http://ameblo.jp/yuki-shinkyu/"><img src="img/common/sblog.png" alt="ブログ" /></a></p>
<p class="sp-mt20"><a href="director.php"><img src="img/common/sintyou.png" alt="院長紹介" /></a></p>

<h4 class="sp-mt40"><img src="img/common/slogo.png" alt="ゆきしんきゅういん" /></h4>
<div class="slogo">
<p>兵庫県西宮市甲子園口北町4-29</p>
<p>マンション三和102号</p>
<p>［ TEL ]　0798-66-8834</p>
</div>
<p><a href="inquiry.php"><img src="img/common/smail.png" alt="ご予約・お問い合わせ" /></a></p>
<p style="margin-top:15px;"><a class="open_help_link" id="open_help_header2" href="javascript:void(0)"><img src="img/common/highquality01.jpg" alt="24時間web予約" width="200" height="111" /></a></p>

<dl class="LinksSide">
	<dt><img src="img/common/link_ttl.png" alt="リンク" /></dt>
    <dd>
    	<ul>
        	<li><a href="http://www.yuki-shinkyu.com/cocolo/index.php" target="_blank"><img src="img/common/link_bnr01.png" alt="cocolo"/></a></li>
    	</ul>
    </dd>
</dl>

</div>

<div id="box_r">
<h3><img src="img/menus/acupun.png" alt="診察時間とアクセス" /></h3>
<p>私たちの体には、体表に経絡（けいらく）というものがあり、その流れにある経穴（ツボのこと）を、鍼や灸で刺激することで、脳に刺激を伝達
させ、鎮痛・自律神経系の調節・血流の改善、免疫強化といった効果、歪み（体の崩れたバランス）を元に戻す（恒常性）働きができます。 
当院の鍼治療は主の整形疾患に対応しスポーツ障害はもちろん、問診で急性・慢性疾患を判断し西洋医学の 処置法(アイシング、テーピング、
温熱療法など)を取り入れながら治療していきます。 症状によっては灸療法も活躍します。</p>
<p class="t-c"><img src="img/menus/katakosi.png" alt="肩こりや腰痛に効果的" width="471" height="166" /></p>
<p class="t-c">その他、「こんな症状は効果がある？」など、一度、ご相談下さい。</p>
<p class="t-c sp-mb30">「ゆきしんきゅう」はあなたの辛い症状に一緒に向き合っていきます。</p>



<div id="menu_tb" class="sp-mb70">
<h3><span>Fee</span>料金</h3>
<table width="770" border="0">
  <tr>
    <td width="301" class="menu01">全身治療<span class="menu_indiva">インディバ込 </span><span class="menu_time">40〜60分</span></td>
    <td width="385" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">5,200円</td>
  </tr>
  </table>
  	<p class="Comment">お悩みの箇所や症状を伺いはり・きゅう・インデイバを使用しトータルケアしていきます。<br />
  	  【主な症状】   腰痛（ギックリ腰）、関節痛（膝、肩）、神経痛（肋間、坐骨、頚、腕）など<br />
  	  その他、肩こり症、顔面麻痺・生理不順・眼精疲労など</p>
    
<table width="770" border="0">
  <tr>
    <td width="301" class="menu01">局所治療<span class="menu_indiva">インディバ込 </span><span class="menu_time">20〜30分</span></td>
    <td width="385" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">4,100円</td>
  </tr>
  </table>
  	<p class="Comment">お悩みの箇所のみをツボをはり・きゅう・インデイバの全てを使用し治療を行います。<br />
  	  【主な症状】 腱鞘炎、捻挫、バネ指、リウマチ（関節痛）など</p>
    
</div>



<h3 class="sp-mt40"><img src="img/menus/indside.png" alt="体の内部と外部から治療" /></h3>
<div id="menu2_boxt">
<div class="menu2_t">
<h4>体を外部から刺激していく治療</h4>
<p>鍼治療、腰痛・肩こり・坐骨神経痛・五十肩・ムチウチ・冷え症・めまい・自律神経失調症・痩身</p>
<p>治療（ダイエット）、スポーツによる故障等その他多くの症状に効果が認められています。</p>
</div>
<div class="menu2_u">
<h4>体の内側から治療してく灸治療</h4>
<p>五臓六腑をお灸による、治療方法。予防医学とも言える治療によって、
内科系疾患や婦人科疾患にまで対応できます。</p>
<p>詳しくは、スタッフまでお気軽にお問合せ下さい。</p>
</div>
</div>

<p class="t-c"><iframe title="YouTube video player" width="440" height="290" src="http://www.youtube.com/embed/E6LMHzLtlkM?rel=0" frameborder="0" allowfullscreen></iframe></p>


<h3 class="sp-mt60"><img src="img/menus/indi.png" alt="適応疾患一覧" width="244" height="17" /></h3>

<div id="menu2_end">
<div class="menu2_end_l">
<h4><img src="img/menus/sik1.png" width="202" height="40" /></h4>
<p>痛めたところをかばっていると他のところも痛くなります。</p>
<p>身体全体のバランスを考えて治療していきます。</p>
</div>
<div class="menu2_end_r">
<h4><img src="img/menus/rik1.png" width="202" height="40" /></h4>
<p>女性にとって冷えは大敵！自律神経やストレスを調整し、冷え
  からくる症状を改善します。 ホルモンバランスの乱れによる更年期障害や生理痛にも鍼灸治療は有効です。</p>

</div>
<div class="cle"></div>

<div class="menu2_end_l">
<h4><img src="img/menus/sik2.png" width="202" height="40" /></h4>
<p>「鍼は刺しません！」　やさしく気持ちよく皮膚を刺激し、<br />
疳虫・夜尿症・小児喘息・アトピーなどを治療します。</p>
</div>
<div class="menu2_end_rr">
<h4><img src="img/menus/rik2.png" width="202" height="40" /></h4>
<p>スポーツでの疲労や故障した身体をツボや経絡を使って<br />
  治療します。</p>

</div>
<div class="cle"></div>


<div class="menu2_end_l">
<h4><img src="img/menus/sik3.png" width="202" height="40" /></h4>
<p>耳ツボの刺激に加え、鍼とお灸でホルモンバランス、胃腸の<br />
働き、新陳代謝を整え、ダイエットをサポートします。</p>
</div>
<div class="menu2_end_rr">
<h4><img src="img/menus/rik3.png" width="202" height="40" /></h4>
<p>神経痛 ・ 神経麻痺 ・ 痙攣 ・ 自律神経失調症 ・ 神経症 ・ 心身症
・ 脳卒中後遺症 ・ 頭痛 ・ 不眠症 ・ 眩暈(めまい) ・ 肩こり</p>

</div>
<div class="cle"></div>

<div class="menu2_end_l">
<h4><img src="img/menus/sik4.png" width="202" height="40" /></h4>
<p>関節炎 ・ 関節症 ・ 肩関節周囲炎(五十肩) ・ 関節リウマチ<br />
筋・筋膜炎(筋肉リウマチ) ・ 頸筋強直 ・ 頸肩腕症 ・ むち打症
・ 捻挫 ・ 腱鞘炎 ・ 腰痛症</p>
</div>
<div class="menu2_end_rr">
<h4><img src="img/menus/rik4.png" width="202" height="40" /></h4>
<p>口内 ・ 炎舌炎 ・ 歯痛 ・ 胃腸炎 ・ 胃アトニー ・ 胃下垂症 ・ 便秘
・ 痔疾患 ・ 胃酸過多症 ・ 胆石症 ・ 肝機能障害 ・ 下痢</p>

</div>
<div class="cle"></div>

<div class="menu2_end_l">
<h4><img src="img/menus/sik5.png" width="202" height="40" /></h4>
<p>感冒 ・ 咳嗽 ・ 鼻炎 ・ 扁桃炎 ・ 咽頭炎 ・ 喉頭炎 ・ 気管支炎 ・
気管支喘息</p>
</div>
<div class="menu2_end_rr">
<h4><img src="img/menus/rik5.png" width="202" height="40" /></h4>
<p>ネフローゼ ・ 腎/尿路結石 ・ 膀胱炎 ・ 尿道炎 ・ 前立腺肥大症
・ 陰萎症 ・ 遺精</p>

</div>
<div class="cle"></div>

<div class="menu2_end_l">
<h4><img src="img/menus/sik6.png" width="202" height="40" /></h4>
<p>月経不順 ・ 月経痛 ・ 冷え症 ・ 更年期障害 ・ 妊娠悪阻(つわり)
・ 胎位異常(さかご) ・ 乳腺炎 ・ 乳汁分泌不全</p>
</div>
<div class="menu2_end_rr">
<h4><img src="img/menus/rik6.png" width="202" height="40" /></h4>
<p>疳虫 ・ 夜啼症 ・ 夜尿症 ・ 自家中毒症 ・ 小児喘息<br />
・ 
  虚弱体質</p>

</div>
<div class="cle"></div>

<div class="menu2_end_l">
<h4><img src="img/menus/sik7.png" width="202" height="40" /></h4>
<p>眼瞼縁炎(ただれめ) ・ 麦粒腫(ものもらい) ・ 結膜炎 ・ <br />
  フリクテン ・ 眼精疲労 ・ 仮性近視 ・ 弱視 ・ 涙管炎</p>
</div>
<div class="menu2_end_rr">
<h4><img src="img/menus/rik7.png" width="202" height="40" /></h4>
<p>耳鳴 ・ 難聴 ・ メニエル病 ・ 中耳炎 ・ 鼻出血(はなぢ) ・ <br />
  副鼻腔炎(蓄膿症)</p>
<div class="cle"></div>
</div>
</div>



<!--ここからインフレ--->
<div class="sp-mt50">
<p class="t-c"><img src="img/biginner/ftlo.png" width="191" height="41" /></p></div>
<ul id="ft1" class="sp-pb20">
    <li><a href="menu1.php"><img src="img/biginner/ft1.png" /></a></li>
    <li><a href="menu2.php"><img src="img/biginner/ft2.png" /></a></li>
    <li><a href="menu3.php"><img src="img/biginner/ft3.png" /></a></li>
</ul>
<ul id="ft1" style="padding:50px 0 0 0;">
    <li><a href="menu4.php"><img src="img/biginner/ft4.png" /></a></li>
</ul>
<div class="cle"></div>
<div id="fts_box">
<ul id="fts">
<li><a href="beginner.php"><img src="img/biginner/fts1.png" /></a></li>
<li><a href="menu.php"><img src="img/biginner/fts2.png" /></a></li>
<li><a href="access.php"><img src="img/biginner/fts3.png" /></a></li>
</ul>
</div>
<div class="cle"></div>

<div id="ft_end">
<div id="ft_end_l">
<h4 class="sp-pl-40"><img src="img/biginner/ff1.png" alt="駐車場のご案内" /></h4>
<div class="sp-pl_t30">
<p>駐車スペースの台数に限りがありますので、<br />
お車でお越しの方はお電話にてご確認下さい。</p>
</div>
</div>


<div id="ft_end_r">
<h4><img src="img/biginner/ff2.png" alt="ご予約・お問い合わせ" /></h4>
<div class="sp-mt20">
<img src="img/biginner/tel.png" alt="TEL0798-66-8834" />
 　　　<a href="inquiry.php"><img src="img/biginner/ftmail.png" width="100" height="28" /></a></div>
</div>
<div class="cle"></div>
</div>

</div><!-- box_r -->




</div><!-- container -->



<?php include("footer.html"); ?>

</body>
</html>
