<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=shift-jis" />
<meta name="keywords" content="西宮市,鍼灸院,美容鍼灸,エステ" />
<meta name="description" content="兵庫県西宮市、JR「甲子園口」駅より徒歩2〜3分のところにある日曜日も開いている鍼灸院です。痩身 治療（ダイエット）、美顔鍼・エステには実績があります。" />
<title>幸鍼灸院（ゆきしんきゅういん）エステ・西宮市、ＪＲ「甲子園口」駅より徒歩２分〜３分</title>
<link href="css/import.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script><!--//back-to-->
<script type="text/javascript" src="js/multihero.js"></script><!--//back-to-->
<script type="text/javascript"> 
// pagetop
$(document).ready(function(){
	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 300);
			return false;
		});
	});

});
</script>

</head>


<body>
	
<div id="header">
    <div class="f-l">
        <h1>西宮のアッとホームな幸鍼灸院</h1>
        <h2><a href="index.php"><img src="img/common/logo.png" alt="ゆきしんきゅういん" /></a></h2>
    </div>
    <div style="float:left; padding-top: 20px;padding-left: 90px;">
        <a class="open_help_link" id="open_help_header" href="javascript:void(0)"><img src="img/common/highqualitymin01.jpg" alt="24時間web予約" width="250" height="67"/></a>
    </div>
    <ul class="f-r">
        <li><a href="inquiry.php"><img src="img/common/inquiry.png" alt="ご予約・お問い合わせ" /></a></li>
        <li><img src="img/common/tel.png" alt="TEL：0798-66-8834" /></li>
    </ul>
    <p id="back-top"><a href="#top"><span></span></a></p>
</div>

<ul id="navi">
    <li><a href="index.php"><img src="img/common/navi01.png" alt="HOME" /></a></li>
    <li><a href="beginner.php"><img src="img/common/navi02.png" alt="初めての方へ" /></a></li>
    <li><a href="menu.php"><img src="img/common/navi03.png" alt="料金" /></a></li>
    <li><a href="access.php"><img src="img/common/navi04.png" alt="診察時間・アクセス" /></a></li>
    <li><a href="director.php"><img src="img/common/navi05.png" alt="院長紹介" /></a></li>
    <li><a href="inquiry.php"><img src="img/common/navi06.png" alt="お問い合わせ" /></a></li>
    <li><a href="http://ameblo.jp/yuki-shinkyu/" target="_blank"><img src="img/common/navi07.png" alt="ブログ" /></a></li>
	<div style="clear:both"></div>
</ul>
  

<div id="container">
<div id="box_l">
<p class="sp-mb50"><a href="beginner.php"><img src="img/common/forbeginner.png" alt="はじめての方へ" width="210" height="210" /></a></p>
<h3><img src="img/common/smenu.png" alt="メニュー" /></h3>
<ul id="smenu">
<li><a href="menu1.php"><img src="img/common/sindiva.png" alt="INDIVA" /></a></li>
<li><a href="menu2.php"><img src="img/common/shari.png" alt="鍼灸" /></a></li>
<li><a href="menu3.php"><img src="img/common/sbiyo.png" alt="美容鍼灸" /></a></li>
<li><a href="menu4.php"><img src="img/common/sseitai.png" alt="美容整骨" /></a></li>
</ul>
<p class="sp-mt40"><img src="img/common/credit.png" alt="クレジットカード使えます" /></a></p>
<p class="sp-mt20"><a href="http://ameblo.jp/yuki-shinkyu/"><img src="img/common/sblog.png" alt="ブログ" /></a></p>
<p class="sp-mt20"><a href="director.php"><img src="img/common/sintyou.png" alt="院長紹介" /></a></p>

<h4 class="sp-mt40"><img src="img/common/slogo.png" alt="ゆきしんきゅういん" /></h4>
<div class="slogo">
<p>兵庫県西宮市甲子園口北町4-29</p>
<p>マンション三和102号</p>
<p>［ TEL ]　0798-66-8834</p>
</div>
<p><a href="inquiry.php"><img src="img/common/smail.png" alt="ご予約・お問い合わせ" /></a></p>
<p style="margin-top:15px;"><a class="open_help_link" id="open_help_header2" href="javascript:void(0)"><img src="img/common/highquality01.jpg" alt="24時間web予約" width="200" height="111" /></a></p>

<dl class="LinksSide">
	<dt><img src="img/common/link_ttl.png" alt="リンク" /></dt>
    <dd>
    	<ul>
        	<li><a href="http://www.yuki-shinkyu.com/cocolo/index.php" target="_blank"><img src="img/common/link_bnr01.png" alt="cocolo"/></a></li>
    	</ul>
    </dd>
</dl>

</div>

<div id="box_r">
<h3><img src="imges/menu1.png" alt="インディバについて" /></h3>
<p>スペイン　インディバ社のカルベット博士が開発した電気メス（高周波）と同レベルの周波数（中波）と機能を応用して完成させた高周波
温熱機器です。従来の高周波領域であった短波〜超短波ではなく、中波領域を使用しております。体表での熱発生を目的としていた従来の
機器と異なり、困難とされていた「深部加温」を可能にしました。頭から足先まで全身にご使用いただけます。
</p>

<div id="menu1_box">
<p>インディバの深部加温は冷えを解消することで<br />
　基礎代謝と免疫力をアップさせさまざまなトラブルを<br />
解決してくれます。</p>
</div>
<p class="t-r"><img src="imges/douga.png" alt="詳細動画はこちら" width="200" height="35" /></p>

<h3><img src="imges/kouka.png" alt="効果" /></h3>
<ul class="menu1_eff">
<li><img src="imges/body.png" alt="身体へのはたらき" /></li>
<li><img src="imges/face.png" alt="お肌へのはたらき" /></li>
<div class="cle"></div>
</ul>
<div class="menu1_pl">
<p>・皮下脂肪、内臓脂肪の自然燃焼の促進
</p>
<p>・冷えの改善、解毒効果、便秘の改善</p>
<p>・首、肩こり、腰痛の緩和</p>
<p>・足の疲れ、むくみの緩和</p>
<p>・全身のシェイプアップ、リフトアップ</p>
<p>・セルライト対策</p>
<p>※女性の場合は生理痛にも効果的</p>
</div>
<div class="menu1_pr">
<p>・しみ、しわ、くすみの解消、アンチエイジング対策</p>
<p>・リフトアップ、小顔・引き締め効果</p>
<p>・老化予防・予防美容</p>
<p>・肌の生まれ変わりを促進</p>

</div>
<div class="cle sp-mb40"></div>



<div id="menu_tb" class="sp-mb70">
<h3><span>Fee</span>料金</h3>
<table width="770" border="0">
  <tr>
    <td width="301" class="menu01">鍼灸治療＋耳ツボ＋インディバスリム<span class="menu_time">70分</span></td>
    <td width="385" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">8,200円</td>
  </tr>
  </table>
    
<table width="770" border="0">
  <tr>
    <td width="338" class="menu01">全身治療 ＋インディバ肩こりマッサージ<span class="menu_time">60〜80分</span></td>
    <td width="348" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">7,800円</td>
  </tr>
  </table>
  	<p class="Comment">全身治療（トータルケア）の施術の後にインディバマッサージ（15分）を行います。<br />
  	  肩こりが特にキツい方にオススメです。</p>
    
<table width="770" border="0">
  <tr>
    <td width="301" class="menu01">美顔はり＋インディバ肩こりマッサージ<span class="menu_time">50分</span></td>
    <td width="385" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">7,500円</td>
  </tr>
  </table>
  	<p class="Comment">美容はりの施術前後にインディバマッサージ（20分）を行い美容効果をアップさせます。
  	</p>
    
<table width="770" border="0">
  <tr>
    <td width="301" class="menu01">インディバ肩こりマッサージ<span class="menu_time">20分</span></td>
    <td width="385" class="menu02">&nbsp;</td>
    <td width="70" class="menu03">3,800円</td>
  </tr>
  </table>
    
</div>



<h3 class="sp-mt40"><img src="img/menu/resu.png" alt="実績" width="147" height="17" /></h3>
<img src="img/menu/menu1.pt.png" width="15" height="15" />
<p class="sp-mb10">
<span class="menu1_zitu">実例１</span>
ダイエットコース　｜　４２歳　　男性　　身長179�p（治療ペースは10日に1回）
</p>
<div class="sp-ml50">
<p>> ２５年４月１８日</p>
<p class="font-color-glay1">体重　78.2�`　　体脂肪　23.0%　　ＢＭＩ24.4　　筋肉量32.7%　　内蔵脂肪9.5%　　皮下脂肪14.3%</p>
<ul class="inline_box1">
<li><img src="img/menu/met1-1.png" width="180" height="120" /></li>
<li><img src="img/menu/met1-2.png" width="180" height="120" /></li>
<li><img src="img/menu/met1-3.png" width="180" height="120" /></li>
</ul>
</div><br />


<div class="sp-ml50">
<p>> ２５年６月２５日</p>
<p class="font-color-glay1">体重　75.0�`　　体脂肪　20.1%　　ＢＭＩ23.4　　筋肉量33.9%　　内蔵脂肪8.0%　　皮下脂肪12.5%</p>
<ul class="inline_box1">
<li><img src="img/menu/met2-1.png" width="180" height="120" /></li>
<li><img src="img/menu/met2-2.png" width="180" height="120" /></li>
<li><img src="img/menu/met2-3.png" width="180" height="120" /></li>
</ul>
</div>
<p class="t-c">へそ上　88.0�p／へそ下　86.0�p</p>

<div class="sp-ml50">
<p>> ２５年８月８日</p>
<p class="font-color-glay1">体重　71.1�`　　体脂肪　17.1%　　ＢＭＩ22.2　　筋肉量35.1%　　内蔵脂肪6.5%　　皮下脂肪10.5%</p>
<ul class="inline_box1">
<li><img src="img/menu/met3-1.png" width="180" height="120" /></li>
<li><img src="img/menu/met3-2.png" width="180" height="120" /></li>
<li><img src="img/menu/met3-3.png" width="180" height="120" /></li>
</ul>
</div>

<p class="sp-mt60"><img src="img/menu/menu1.pt.png" width="15" height="15" /></p>
<p class="sp-mb10">
<span class="menu1_zitu">実例2</span>
ダイエットコース　｜　４０歳代　　女性
</p>
<div class="sp-ml50">
<p>> ２０１２年　９月</p>
<p class="font-color-glay1">【ウエスト】　８１.５�p 　【下腹部】　９２�p</p>
<ul class="inline_box1">
<li><img src="img/menu/det1-1.png" width="140" height="180" /></li>
<li><img src="img/menu/det1-2.png" width="140" height="180" /></li>
</ul>
</div>

<div class="sp-ml50">
<p>> ２０１３年　１月</p>
<p class="font-color-glay1">【ウエスト】　７６�p　マイナス　５.５�p 　【下腹部】　８５�p　マイナス５�p</p>
<ul class="inline_box1">
<li><img src="img/menu/det2-1.png" width="140" height="180" /></li>
<li><img src="img/menu/det2-2.png" width="140" height="180" /></li>
</ul>
</div>

<div class="sp-ml50">
<p>> ２０１３年　１月</p>
<p class="font-color-glay1">【ウエスト】　７６�p　マイナス　５.５�p 　【下腹部】　８５�p　マイナス５�p</p>
<ul class="inline_box1">
<li><img src="img/menu/det3-1.png" width="140" height="180" /></li>
<li><img src="img/menu/det3-2.png" width="140" height="180" /></li>
</ul>
</div>
<!--
<div class="inline_box1_s">

<h3><img src="img/menu/movie.png" alt="INDIVA詳細動画" width="209" height="17" /></h3>
<img src="img/menu/indiva_me1.png" alt="ＩＮＤＩＶＡをもっとよく知ろう！" width="330" height="20" /> 
<div class="cle"></div>
</div>
<table width="770" cellpadding="10">
  <tr>
    <td>下股の関節・筋肉の治療とケア</td>
    <td>肩こりと首の治療とケア</td>
  </tr>
  <tr>
    <td><img name="" src="" width="360" height="240" alt="" /></td>
    <td><img name="" src="" width="360" height="240" alt="" /></td>
  </tr>
</table>

<table width="770" cellpadding="10">
  <tr>
    <td>背中と腰の治療とケア</td>
    <td>INDIBAの全て</td>
  </tr>
  <tr>
    <td><img name="" src="" width="360" height="240" alt="" /></td>
    <td><img name="" src="" width="360" height="240" alt="" /></td>
  </tr>
</table>
-->

<div class="sp-mt50">
  <p class="t-c"><img src="img/biginner/ftlo.png" width="191" height="41" /></p></div>
<div class="sp-mt50">
<p class="t-c"><img src="img/biginner/ftlo.png" width="191" height="41" /></p></div>
<ul id="ft1" class="sp-pb20">
    <li><a href="menu1.php"><img src="img/biginner/ft1.png" /></a></li>
    <li><a href="menu2.php"><img src="img/biginner/ft2.png" /></a></li>
    <li><a href="menu3.php"><img src="img/biginner/ft3.png" /></a></li>
</ul>
<ul id="ft1" style="padding:50px 0 0 0;">
    <li><a href="menu4.php"><img src="img/biginner/ft4.png" /></a></li>
</ul>
<div class="cle"></div>
<div id="fts_box">
<ul id="fts">
<li><a href="beginner.php"><img src="img/biginner/fts1.png" /></a></li>
<li><a href="menu.php"><img src="img/biginner/fts2.png" /></a></li>
<li><a href="access.php"><img src="img/biginner/fts3.png" /></a></li>
</ul>
</div>
<div class="cle"></div>

<div id="ft_end">
<div id="ft_end_l">
<h4 class="sp-pl-40"><img src="img/biginner/ff1.png" alt="駐車場のご案内" /></h4>
<div class="sp-pl_t30">
<p>駐車スペースの台数に限りがありますので、<br />
お車でお越しの方はお電話にてご確認下さい。</p>
</div>
</div>


<div id="ft_end_r">
<h4><img src="img/biginner/ff2.png" alt="ご予約・お問い合わせ" /></h4>
<div class="sp-mt20">
<img src="img/biginner/tel.png" alt="TEL0798-66-8834" />
 　　　<a href="inquiry.php"><img src="img/biginner/ftmail.png" width="100" height="28" /></a></div>
</div>
<div class="cle"></div>
</div>

</div><!-- box_r -->




</div><!-- container -->



<?php include("footer.html"); ?>

</body>
</html>
