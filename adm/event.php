<?php
require_once("class.php");
// カレントの言語を日本語に設定する
mb_language("Japanese");
// 内部文字エンコードを設定する
mb_internal_encoding("shift_jis");
// 日時を取得
$y = date("Y");
$m = date("m");
$d = date("d");
$h = date("H");
$i = date("i");
$s = date("s");
// 初期化
$ttl = "";
$evday = "";
$place = "";
$cost = "";
$num = "";
$cond = "";
$msg = "";
$ttlerr = "";
$evdayerr = "";
$placeerr = "";
$costerr = "";
$numerr = "";
$conderr = "";
$msgerr = "";
$imgerr = "";
//リサイズの横幅
$chwidth = "700";
//-------------------------------------------------------
//　POSTされたとき
//-------------------------------------------------------
if($_SERVER['REQUEST_METHOD']=="POST"){
if(isset($_POST["submit"])){
	$flg = "";
	//-----------------------------------------------
	//　POSTされたデータを取得
	//-----------------------------------------------
	//　新規追加
	$ttl = htmlspecialchars($_POST["ttl"],ENT_QUOTES);
	$ttl = mb_convert_encoding($ttl,"SJIS","ASCII,JIS,UTF-8,EUC-JP,SJIS");
	$evday = htmlspecialchars($_POST["evday"],ENT_QUOTES);
	$evday = mb_convert_encoding($evday,"SJIS","ASCII,JIS,UTF-8,EUC-JP,SJIS");
	$place = htmlspecialchars($_POST["place"],ENT_QUOTES);
	$place = mb_convert_encoding($place,"SJIS","ASCII,JIS,UTF-8,EUC-JP,SJIS");
	$cost = htmlspecialchars($_POST["cost"],ENT_QUOTES);
	$cost = mb_convert_encoding($cost,"SJIS","ASCII,JIS,UTF-8,EUC-JP,SJIS");
	$num = htmlspecialchars($_POST["num"],ENT_QUOTES);
	$num = mb_convert_encoding($num,"SJIS","ASCII,JIS,UTF-8,EUC-JP,SJIS");
	$cond = htmlspecialchars($_POST["cond"],ENT_QUOTES);
	$cond = mb_convert_encoding($cond,"SJIS","ASCII,JIS,UTF-8,EUC-JP,SJIS");
	$msg = htmlspecialchars($_POST["msg"],ENT_QUOTES);
	$msg = mb_convert_encoding($msg,"SJIS","ASCII,JIS,UTF-8,EUC-JP,SJIS");
	
	//　画像用
	$name = $_FILES["img"]["name"];
	$tmp = $_FILES["img"]["tmp_name"];
	$type = $_FILES["img"]["type"];
	$size = $_FILES["img"]["size"];
	
	//-----------------------------------------------
	//　チェック
	//-----------------------------------------------
	$check = new upcheck();
	//　ttl
	if($ttl == ""){
		$ttlerr = "<br /><div class=\"fol\">タイトルを記入してください。</div>";
		$flg = "1";
	}
	//　evaday
	if($evday == ""){
		$evdayerr = "<br /><div class=\"fol\">日時を記入してください。</div>";
		$flg = "1";
	}
	//　place
	if($place == ""){
		$placeerr = "<br /><div class=\"fol\">場所を記入してください。</div>";
		$flg = "1";
	}
	//　cost
	if($cost == ""){
		$costerr = "<br /><div class=\"fol\">参加費を記入してください。</div>";
		$flg = "1";
	}
	//　num
	if($num == ""){
		$numerr = "<br /><div class=\"fol\">定員数を記入してください。</div>";
		$flg = "1";
	}
	//　cond
	if($cond == ""){
		$conderr = "<br /><div class=\"fol\">資格を記入してください。</div>";
		$flg = "1";
	}
	//　msg
	if($msg == ""){
		$msgerr = "<br /><div class=\"fol\">内容本文を記入してください。</div>";
		$flg = "1";
	}
	
	if($flg == ""){
		//------------------------------------------------------------------------------------------------------
		//　アップロード
		//------------------------------------------------------------------------------------------------------
		$dirpass = getcwd();
		$dirnm = $y.$m.$d.$h.$i.$s;
		$dir = $dirpass."/data/".$dirnm;
		if(file_exists($dir)==false){
			mkdir($dir, 0777);
			chmod ($dir, 0777);
		}
		//　画像アップ
		if(strlen($name) > 0 && strlen($tmp) > 0){
			if(is_uploaded_file($tmp)){
				if($type == "image/gif"){
					move_uploaded_file($tmp, $dir."/img.gif");
					list($width, $height) = getimagesize($dir."/img.gif");
					if($width > 700){
						$img = new Image($dir."/img.gif");
						$img->width($chwidth);
						$img->save();
					}
					chmod($dir."/img.gif", 0777);
				}
				if($type == "image/jpeg" || $type == "image/pjpeg"){
					move_uploaded_file($tmp, $dir."/img.jpg");
					list($width, $height) = getimagesize($dir."/img.jpg");
					if($width > 700){
						$img = new Image($dir."/img.jpg");
						$img->width($chwidth);
						$img->save();
					}
					chmod($dir."/img.jpg", 0777);
				}
			}
		}
		//　テキスト
		$txt .= $ttl."\n";
		$txt .= $evday."\n";
		$txt .= $place."\n";
		$txt .= $cost."\n";
		$txt .= $num."\n";
		$txt .= $cond."\n";
		$txt .= $msg;
		if ( get_magic_quotes_gpc( ) ) {
		    $txt = stripslashes($txt);
		}
		
		// テキスト書き込み
		$filenm = "data.cgi";
		$txtnm = $dir."/".$filenm;
		$fp = @fopen($txtnm,"a");
		if(!$fp){
			exit ("ファイル書き込みのオープンに失敗");
		}else{
			flock($fp,LOCK_EX);
			fwrite($fp,$txt);
			flock($fp,LOCK_UN);
			fclose($fp);
			chmod($txtnm, 0777);
		}
		header("Location: event.php");
		exit;
	}
}else{
	foreach($_POST as $key => $value){
		if(preg_match("/[0-9]{13}/",$key)){
			$deldir = $key;
		}
	}
	$cgipass = "./data/".$deldir."/data.cgi";
	$jpgpass = "./data/".$deldir."/img.jpg";
	$gifpass = "./data/".$deldir."/img.gif";
	$dir = "./data/".$deldir;
	if(file_exists($cgipass) === true){
		$od = opendir($dir);
		unlink($cgipass);
		closedir($od);
	}
	if(file_exists($jpgpass) === true){
		$od = opendir($dir);
		unlink($jpgpass);
		closedir($od);
	}
	if(file_exists($gifpass) === true){
		$od = opendir($dir);
		unlink($gifpass);
		closedir($od);
	}
	rmdir($dir);
}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=shift-jis" />
<title>パーティ・イベント情報</title>
<link rel=stylesheet href="css/adm.css" type="text/css">
</head>

<body>
<div id="container">

<!--header-->
<div id="header">
<div id="box">
	<div class="hdmn01"><a href="news.php"><img src="image/admin_hdbtn01.gif" /></a></div>
	<div class="hdmn02"><img src="image/admin_hdbtn02.gif" /></div>
</div>
</div>

<div><img src="image/admin_event_ttl.gif" /></div>

<!--contents-->
<div id="content">

<!--新規-->
<div class="pb20"><img src="image/admin_ttl01.gif" /></div>
<!--form-->
<form action="<?=$_SERVER['PHP_SELF']?>" method="post" enctype="multipart/form-data">
	<div><img src="image/contact_form01.gif" /></div>
	<div id="formbox">

		<div id="box">
			<div class="formevttl">管理画面用タイトル</div>
			<div class="formevinp"><input name="ttl" type="text" value="<?=$ttl?>" size="80" /><?=$ttlerr?>
			</div>
		</div>

		<div id="box">
			<div class="formevttl">ホームページ用タイトル</div>
			<div class="formevinp"><input name="evday" type="text" value="<?=$evday?>" size="80" /><?=$evdayerr?>
			</div>
		</div>

		<div id="box">
			<div class="formevttl">イベント内容</div>
			<div class="formevinp"><input name="place" type="text" value="<?=$place?>" size="80" /><?=$placeerr?>
			</div>
		</div>

		<div id="box">
			<div class="formevttl">日　時</div>
			<div class="formevinp"><input name="cost" type="text" value="<?=$cost?>" size="80" /><?=$costerr?>
			</div>
		</div>

		<div id="box">
			<div class="formevttl">費　用</div>
			<div class="formevinp"><input name="num" type="text" value="<?=$num?>" size="80" /><?=$numerr?>
			</div>
		</div>

		<div id="box">
			<div class="formevttl">定　員</div>
			<div class="formevinp"><input name="cond" type="text" value="<?=$cond?>" size="80" /><?=$conderr?>
			</div>
		</div>

		

		<div id="box">
			<div class="formevttl">備　考</div>
			<div class="formevinp"><textarea name="msg" cols="85" rows="5" style="font-size:12px"><?=$msg?></textarea>
			<?=$msgerr?></div>
		</div>
		
		<div id="box">
			<div class="formevttl"></div>
			<div class="formbtnb"><input type="submit" name="submit" value="登 録" /></div>
		</div>

	</div>
	<div class="pb30"><img src="image/contact_form03.gif" /></div>

<!--確認・削除-->
<div class="pb20"><img src="image/admin_ttl02.gif" /></div>

<?php
//内容確認
$dir = "./data/";
$dirs = array();
//ディレクトリを開いて変数に代入
$od = opendir($dir);
while(false !== ($list = readdir($od))){
	if(preg_match("/[0-9]{13}/",$list)){
		array_push($dirs, $list);
	}
}
closedir($od);
rsort($dirs);
//　更新情報ファイルを開いてテーブルタグ追加表示
foreach($dirs as $one){		//一件づつ【onelist】に送る
	echo onelist($one);
}
//　テーブルタグで表示
function onelist($one){
	$check = new upcheck();
	$pass = "./data/".$one."/data.cgi";
	$fo = fopen($pass,"r");
	while(!feof($fo)){
		$data[] = fgets($fo);
	}
	fclose($fo);
	$tags = "";
	$tags .= "<div id=\"box\">\n";
	$tags .= "	<div class=\"formdelttl\">";
	foreach($data as $key => $value){
		if($key==0){
			$value = $check->conv($value);
			$tags.= $value;
		}
	}
	$tags .= "</div>\n";
	$tags .= "	<div class=\"formbtn\">";
	$tags .= "<input type=\"submit\" name=\"{$one}\" value=\"削 除\" />";
	$tags .= "	</div>\n";
	$tags .= "</div>\n";
	$tags .= "<div class=\"pb5\"><img src=\"image/admin_line.gif\" /></div>\n";
	return $tags;
}
?>

</form>

</div>
<!--footer-->
<div id="ftmn"><a href="news.php">News &amp; Topics</a>｜パーティ・イベント情報</div>
<div class="pb20"><img src="image/tmp_ft02.gif" /></div>

</div>
</body>
</html>