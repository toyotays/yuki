<?php
//============================================
//	チェック用クラス
//============================================
class upcheck{
	//----------------------------------------
	//　変数の宣言
	//----------------------------------------
	var $str = "";
	var $imgerr = "";
	function upcheck(){}
	
	function conv($str){
		$this->str = mb_convert_encoding($str,"SJIS","ASCII,JIS,UTF-8,EUC-JP,SJIS");
		return $this->str;
	}
	
	function chpic($name,$tmp,$type,$size){
		if(strlen($name)>0){
			if(is_uploaded_file($tmp)){
				$kaku = "";
				if($size == 0){$this->imgerr = "<br /><div class=\"fol\">画像が不正です</div>";}
				if($size>20971520){$this->imgerr = "<br /><div class=\"fol\">画像のサイズが大き過ぎます({$pic_size}バイト)</div>";}
				if($type == "image/gif"){$kaku = "gif";}
				if($type == "image/jpeg" || $type == "image/pjpeg"){$kaku = "jpg";}
				if($kaku == ""){$this->imgerr = "<br /><div class=\"fol\">登録画像は【JPG】か【GIF】以外使用できません。</div>";}
			}
		}else{
			$this->imgerr = "<br /><div class=\"fol\">画像が未選択です</div>";
		}
		return $this->imgerr;
	}
	
	function image_resize($source, $length){
  		//元画像のサイズをゲット
		list($o_width, $o_height) = getimagesize($source);
		//縦長か横長かをチェック
		if($o_width > $o_height){//横長の場合
			$height = ($o_height / $o_width) * $length;
			$width = $length;
		}else{//縦長の場合
			$width = ($o_width / $o_height) * $length;
			$height = $length;
		}
		//元画像を読み込み
		$source = imagecreatefromjpeg($source);
		//リサイズ用の画像を作成
		$new_image = imagecreatetruecolor($width,$height);
		//元画像からリサイズしてコピー
		imagecopyresampled($new_image,$source, 0, 0, 0, 0,
		$width, $height, $o_width, $o_height);
		//画像の表示
		header('Content-Type: image/jpeg');
		imagejpeg($new_image,NULL,100);
		imagedestroy($new_image);
	}
}
//============================================
//	リサイズ用クラス
//============================================
class Image {
    
    var $file;
    var $image_width;
    var $image_height;
    var $width;
    var $height;
    var $ext;
    var $types = array('','gif','jpeg','png','swf');
    var $quality = 80;
    var $top = 0;
    var $left = 0;
    var $crop = false;
    var $type;
    
    function Image($name='') {
        $this->file = $name;
        $info = getimagesize($name);
        $this->image_width = $info[0];
        $this->image_height = $info[1];
        $this->type = $this->types[$info[2]];
        $info = pathinfo($name);
        $this->dir = $info['dirname'];
        $this->name = str_replace('.'.$info['extension'], '', $info['basename']);
        $this->ext = $info['extension'];
    }
    
    function dir($dir='') {
        if(!$dir) return $this->dir;
        $this->dir = $dir;
    }
    
    function name($name='') {
        if(!$name) return $this->name;
        $this->name = $name;
    }
    
    function width($width='') {
        $this->width = $width;
    }
    
    function height($height='') {
        $this->height = $height;
    }
    
    function resize($percentage=50) {
        if($this->crop) {
            $this->crop = false;
            $this->width = round($this->width*($percentage/100));
            $this->height = round($this->height*($percentage/100));
            $this->image_width = round($this->width/($percentage/100));
            $this->image_height = round($this->height/($percentage/100));
        } else {
            $this->width = round($this->image_width*($percentage/100));
            $this->height = round($this->image_height*($percentage/100));
        }
        
    }
    
    function crop($top=0, $left=0) {
        $this->crop = true;
        $this->top = $top;
        $this->left = $left;
    }
    
    function quality($quality=80) {
        $this->quality = $quality;
    }
    
    function show() {
        $this->save(true);
    }
    
    function save($show=false) {

        if($show) @header('Content-Type: image/'.$this->type);
        
        if(!$this->width && !$this->height) {
            $this->width = $this->image_width;
            $this->height = $this->image_height;
        } elseif (is_numeric($this->width) && empty($this->height)) {
            $this->height = round($this->width/($this->image_width/$this->image_height));
        } elseif (is_numeric($this->height) && empty($this->width)) {
            $this->width = round($this->height/($this->image_height/$this->image_width));
        } else {
            if($this->width<=$this->height) {
                $height = round($this->width/($this->image_width/$this->image_height));
                if($height!=$this->height) {
                    $percentage = ($this->image_height*100)/$height;
                    $this->image_height = round($this->height*($percentage/100));
                }
            } else {
                $width = round($this->height/($this->image_height/$this->image_width));
                if($width!=$this->width) {
                    $percentage = ($this->image_width*100)/$width;
                    $this->image_width = round($this->width*($percentage/100));
                }
            }
        }
        
        if($this->crop) {
            $this->image_width = $this->width;
            $this->image_height = $this->height;
        }

        if($this->type=='jpeg') $image = imagecreatefromjpeg($this->file);
        if($this->type=='png') $image = imagecreatefrompng($this->file);
        if($this->type=='gif') $image = imagecreatefromgif($this->file);
        
        $new_image = imagecreatetruecolor($this->width, $this->height);
        imagecopyresampled($new_image, $image, 0, 0, $this->top, $this->left, $this->width, $this->height, $this->image_width, $this->image_height);
        
        $name = $show ? null: $this->dir.DIRECTORY_SEPARATOR.$this->name.'.'.$this->ext;
        if($this->type=='jpeg') imagejpeg($new_image, $name, $this->quality);
        if($this->type=='png') imagepng($new_image, $name);
        if($this->type=='gif') imagegif($new_image, $name);
        
        imagedestroy($image); 
        imagedestroy($new_image);
        
    }
    
}
?>