<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=shift-jis" />
<meta name="keywords" content="西宮市,鍼灸院,美容鍼灸,エステ" />
<meta name="description" content="兵庫県西宮市、JR「甲子園口」駅より徒歩2〜3分のところにある日曜日も開いている鍼灸院です。痩身 治療（ダイエット）、美顔鍼・エステには実績があります。" />
<title>幸鍼灸院（ゆきしんきゅういん）エステ・西宮市、ＪＲ「甲子園口」駅より徒歩２分〜３分</title>
<link href="css/import.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script><!--//back-to-->
<script type="text/javascript" src="js/multihero.js"></script><!--//back-to-->
<script type="text/javascript"> 
// pagetop
$(document).ready(function(){
	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 300);
			return false;
		});
	});

});
</script>

</head>


<body>
	
<div id="header">
    <div class="f-l">
        <h1>西宮のアッとホームな幸鍼灸院</h1>
        <h2><a href="index.php"><img src="img/common/logo.png" alt="ゆきしんきゅういん" /></a></h2>
    </div>
    <div style="float:left; padding-top: 20px;padding-left: 90px;">
        <a class="open_help_link" id="open_help_header" href="javascript:void(0)"><img src="img/common/highqualitymin01.jpg" alt="24時間web予約" width="250" height="67"/></a>
    </div>
    <ul class="f-r">
        <li><a href="inquiry.php"><img src="img/common/inquiry.png" alt="ご予約・お問い合わせ" /></a></li>
        <li><img src="img/common/tel.png" alt="TEL：0798-66-8834" /></li>
    </ul>
    <p id="back-top"><a href="#top"><span></span></a></p>
</div>

<ul id="navi">
    <li><a href="index.php"><img src="img/common/navi01.png" alt="HOME" /></a></li>
    <li><a href="beginner.php"><img src="img/common/navi02.png" alt="初めての方へ" /></a></li>
    <li><a href="menu.php"><img src="img/common/navi03.png" alt="料金" /></a></li>
    <li><a href="access.php"><img src="img/common/navi04.png" alt="診察時間・アクセス" /></a></li>
    <li><a href="director.php"><img src="img/common/navi05.png" alt="院長紹介" /></a></li>
    <li><a href="inquiry.php"><img src="img/common/navi06.png" alt="お問い合わせ" /></a></li>
    <li><a href="http://ameblo.jp/yuki-shinkyu/" target="_blank"><img src="img/common/navi07.png" alt="ブログ" /></a></li>
	<div style="clear:both"></div>
</ul>
  

<div id="container">

<h3><img src="img/top/h3.png" alt="アットホームな鍼灸院" /></h3> 


<div class="top_main">
	<div class="left">
    	<dl class="beginner">
        	<dt><h4><img src="img/top/beginner_ttl.png" alt="はじめての方へ" /></h4></dt>
        	<dd><a href="beginner.php"><img src="img/top/beginner_bnr.png" alt="東洋医学からはじまる心地の良い時間" /></a></dd>
        </dl>
        <ul>
        	<li><a href="http://ameblo.jp/yuki-shinkyu/" target="_blank"><img src="img/top/blog_bnr.png" /></a></li>
        	<li class="sp-pl20"><a href="director.php"><img src="img/top/intro_bnr.png" /></a></li>
        </ul>
    </div>
    <div class="right">
    	<ul>
        	<h4><img src="img/top/menu_ttl.png" /></h4>
        	<div>
                <li><a href="menu1.php"><img src="img/top/menu_navi01.png" alt="INDIBA" /></a></li>
                <li><a href="menu2.php"><img src="img/top/menu_navi02.png" alt="鍼灸" /></a></li>
            </div>
        	<div>
                <li><a href="menu3.php"><img src="img/top/menu_navi03.png" alt="美容鍼灸" /></a></li>
                <li><a href="menu4.php"><img src="img/top/menu_navi04.png" alt="美容整骨" /></a></li>
            </div>
        </ul>
        <div class="clear">
            <p class="f-l"><a href="access.php"><img src="img/top/access_bnr.png" /></a></p>
            <dl class="f-r">
                <dt>駐車場のご案内</dt>
                    <dd class="parking">駐車スペースの台数に限りがありますので、<br />
                    お車でお越しの方はお電話にてご確認下さい。</dd>
                <dt>ご予約・お問い合わせ</dt>
                    <dd><p><img src="img/top/booking_tel.png" /></p><p class="sp-pl10"><a href="inquiry.php"><img src="img/top/booking_mail.png" /></a></p>
                    </dd>
            </dl>
         </div>
    </div>
</div> 
</div><!-- container -->
<p style="width:1000px; margin:30px auto 0;"><a href="recruit.php"><img src="img/recruit_bnr.png" /></a></p>

<?php include("footer.html"); ?>


</body>
</html>
