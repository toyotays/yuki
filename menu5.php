<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=shift-jis" />
<meta name="keywords" content="西宮市,鍼灸院,美容鍼灸,エステ" />
<meta name="description" content="兵庫県西宮市、JR「甲子園口」駅より徒歩2〜3分のところにある日曜日も開いている鍼灸院です。痩身 治療（ダイエット）、美顔鍼・エステには実績があります。" />
<title>幸鍼灸院（ゆきしんきゅういん）エステ・西宮市、ＪＲ「甲子園口」駅より徒歩２分〜３分</title>
<link href="css/import.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script><!--//back-to-->
<script type="text/javascript" src="js/multihero.js"></script><!--//back-to-->
<script type="text/javascript"> 
// pagetop
$(document).ready(function(){
	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 300);
			return false;
		});
	});

});
</script>

</head>


<body>
	
<div id="header">
    <div class="f-l">
        <h1>西宮のアッとホームな幸鍼灸院</h1>
        <h2><a href="index.php"><img src="img/common/logo.png" alt="ゆきしんきゅういん" /></a></h2>
    </div>
    <div style="float:left; padding-top: 20px;padding-left: 90px;">
        <a class="open_help_link" id="open_help_header" href="javascript:void(0)"><img src="img/common/highqualitymin01.jpg" alt="24時間web予約" width="250" height="67"/></a>
    </div>
    <ul class="f-r">
        <li><a href="inquiry.php"><img src="img/common/inquiry.png" alt="ご予約・お問い合わせ" /></a></li>
        <li><img src="img/common/tel.png" alt="TEL：0798-66-8834" /></li>
    </ul>
    <p id="back-top"><a href="#top"><span></span></a></p>
</div>

<ul id="navi">
    <li><a href="index.php"><img src="img/common/navi01.png" alt="HOME" /></a></li>
    <li><a href="beginner.php"><img src="img/common/navi02.png" alt="初めての方へ" /></a></li>
    <li><a href="menu.php"><img src="img/common/navi03.png" alt="料金" /></a></li>
    <li><a href="access.php"><img src="img/common/navi04.png" alt="診察時間・アクセス" /></a></li>
    <li><a href="director.php"><img src="img/common/navi05.png" alt="院長紹介" /></a></li>
    <li><a href="inquiry.php"><img src="img/common/navi06.png" alt="お問い合わせ" /></a></li>
    <li><a href="http://ameblo.jp/yuki-shinkyu/" target="_blank"><img src="img/common/navi07.png" alt="ブログ" /></a></li>
	<div style="clear:both"></div>
</ul>
  

<div id="container">
<div id="box_l">
<p class="sp-mb50"><a href="beginner.php"><img src="img/common/forbeginner.png" alt="はじめての方へ" width="210" height="210" /></a></p>
<h3><img src="img/common/smenu.png" alt="メニュー" /></h3>
<ul id="smenu">
<li><a href="menu1.php"><img src="img/common/sindiva.png" alt="INDIVA" /></a></li>
<li><a href="menu2.php"><img src="img/common/shari.png" alt="鍼灸" /></a></li>
<li><a href="menu3.php"><img src="img/common/sbiyo.png" alt="美容鍼灸" /></a></li>
<li><a href="menu4.php"><img src="img/common/sseitai.png" alt="美容整骨" /></a></li>
</ul>

<p class="sp-mt40"><img src="img/common/credit.png" alt="クレジットカード使えます" /></a></p>
<p class="sp-mt20"><a href="http://ameblo.jp/yuki-shinkyu/"><img src="img/common/sblog.png" alt="ブログ" /></a></p>
<p class="sp-mt20"><a href="director.php"><img src="img/common/sintyou.png" alt="院長紹介" /></a></p>

<h4 class="sp-mt40"><img src="img/common/slogo.png" alt="ゆきしんきゅういん" /></h4>
<div class="slogo">
<p>兵庫県西宮市甲子園口北町4-29</p>
<p>マンション三和102号</p>
<p>［ TEL ]　0798-66-8834</p>
</div>
<p><a href="inquiry.php"><img src="img/common/smail.png" alt="ご予約・お問い合わせ" /></a></p>
<p style="margin-top:15px;"><a class="open_help_link" id="open_help_header2" href="javascript:void(0)"><img src="img/common/highquality01.jpg" alt="24時間web予約" width="200" height="111" /></a></p>

<dl class="LinksSide">
	<dt><img src="img/common/link_ttl.png" alt="リンク" /></dt>
    <dd>
    	<ul>
        	<li><a href="http://www.yuki-shinkyu.com/cocolo/index.php" target="_blank"><img src="img/common/link_bnr01.png" alt="cocolo"/></a></li>
    	</ul>
    </dd>
</dl>

</div>

<div id="box_r">
<h3 class="sp-mb40"><img src="img/menu05/h3.png" alt="Focus-D" /></h3>

<div class="clear sp-mb50">
	<p class="f-l sp-mr20"><img src="img/menu05/intro_img.png" /></p>
    <p class="f-l sp-mt10">一般的な痩身機器では、脂肪細胞に届く熱量が少ない為、<br />
    脂肪細胞が溶ける量が少ないのが現状です。<br />
    FOCUS-Dは、それを打破。<br />
    痛みも無く、確実に効果を実感していただけるのが特徴です。
    </p>
</div>

<p class="sp-mb50"><img src="img/menu05/img01.png" /></p>

<p class="sp-mb100"><img src="img/menu05/price.png" /></p>


	<p class="sp-mb30"><img src="img/menu05/ttl01.png" /></p>
	<dl class="AttentionMenu02">
    	<dt>＜禁忌事項＞</dt>
        <dd>
        	<ul>
            	<li>金属やシリコン製のプロテーゼを施術部体内に使用されている方</li>
            	<li>妊娠中、授乳中、もしくは妊娠の可能性のある方</li>
            	<li>術後直後、入院治療直後の方</li>
            	<li>施術部位にヘルニアのある方や外傷のある方</li>
            	<li>医療電子機器（ペースメーカー等）を使用されている方</li>
            	<li>重篤な疾患のある方</li>
            	<li>てんかんの既往症のある方</li>
            	<li>18歳以下の成長期の方</li>
            	<li>脂肪が２cm以上ない部位</li>
            </ul>
        </dd>
    	<dt>＜注意事項＞</dt>
        <dd>
        	<ul>
            	<li>怪我や火傷をされている箇所のトリートメントは避けて下さい。</li>
            	<li>お顔・バストのトリートメントは避けて下さい。</li>
            	<li>施術箇所にクリームやオイル等をつけないで下さい。(施術前）</li>
            </ul>
        </dd>
    	<dt>＜施術効果を高めるために＞</dt>
        <dd>
        	<ul>
            	<li>施術後１時間は食事を控えて下さい。</li>
            	<li>糖分、脂分の過剰摂取は控えて下さい。</li>
            	<li>アルコールの摂取を控えて下さい。</li>
            	<li>代謝を高めるために適度な運動を心がけて下さい。</li>
            	<li>利尿作用のあるものを摂取することをお勧めします。</li>
            </ul>
        </dd>
    </dl>



<div class="sp-mt50">
  <p class="t-c"><img src="img/biginner/ftlo.png" width="191" height="41" /></p></div>
<ul id="ft1">
<li><a href="menu1.php"><img src="img/biginner/ft1.png" /></a></li>
<li><a href="menu2.php"><img src="img/biginner/ft2.png" /></a></li>
<li><a href="menu3.php"><img src="img/biginner/ft3.png" /></a></li>
</ul>
<ul id="ft1" style="padding:50px 0 0 100px;">
    <li><a href="menu4.php"><img src="img/biginner/ft4.png" /></a></li>
    <li><a href="menu5.php"><img src="img/biginner/ft5.png" /></a></li>
</ul>
<div class="cle"></div>
<div id="fts_box">
<ul id="fts">
<li><a href="beginner.php"><img src="img/biginner/fts1.png" /></a></li>
<li><a href="menu.php"><img src="img/biginner/fts2.png" /></a></li>
<li><a href="access.php"><img src="img/biginner/fts3.png" /></a></li>
</ul>
</div>
<div class="cle"></div>

<div id="ft_end">
<div id="ft_end_l">
<h4 class="sp-pl-40"><img src="img/biginner/ff1.png" alt="駐車場のご案内" /></h4>
<div class="sp-pl_t30">
<p>駐車スペースの台数に限りがありますので、<br />
お車でお越しの方はお電話にてご確認下さい。</p>
</div>
</div>


<div id="ft_end_r">
<h4><img src="img/biginner/ff2.png" alt="ご予約・お問い合わせ" /></h4>
<div class="sp-mt20">
<img src="img/biginner/tel.png" alt="TEL0798-66-8834" />
 　　　<a href="inquiry.php"><img src="img/biginner/ftmail.png" width="100" height="28" /></a></div>
</div>
<div class="cle"></div>
</div>

</div><!-- box_r -->




</div><!-- container -->



<?php include("footer.html"); ?>

</body>
</html>
