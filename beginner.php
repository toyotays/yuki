<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=shift-jis" />
<meta name="keywords" content="西宮市,鍼灸院,美容鍼灸,エステ" />
<meta name="description" content="兵庫県西宮市、JR「甲子園口」駅より徒歩2〜3分のところにある日曜日も開いている鍼灸院です。痩身 治療（ダイエット）、美顔鍼・エステには実績があります。" />
<title>幸鍼灸院（ゆきしんきゅういん）エステ・西宮市、ＪＲ「甲子園口」駅より徒歩２分〜３分</title>
<link href="css/import.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script><!--//back-to-->
<script type="text/javascript" src="js/multihero.js"></script><!--//back-to-->
<script type="text/javascript"> 
// pagetop
$(document).ready(function(){
	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 300);
			return false;
		});
	});

});
</script>

</head>


<body>
	
<div id="header">
    <div class="f-l">
        <h1>西宮のアッとホームな幸鍼灸院</h1>
        <h2><a href="index.php"><img src="img/common/logo.png" alt="ゆきしんきゅういん" /></a></h2>
    </div>
    <div style="float:left; padding-top: 20px;padding-left: 90px;">
        <a class="open_help_link" id="open_help_header" href="javascript:void(0)"><img src="img/common/highqualitymin01.jpg" alt="24時間web予約" width="250" height="67"/></a>
    </div>
    <ul class="f-r">
        <li><a href="inquiry.php"><img src="img/common/inquiry.png" alt="ご予約・お問い合わせ" /></a></li>
        <li><img src="img/common/tel.png" alt="TEL：0798-66-8834" /></li>
    </ul>
    <p id="back-top"><a href="#top"><span></span></a></p>
</div>

<ul id="navi">
    <li><a href="index.php"><img src="img/common/navi01.png" alt="HOME" /></a></li>
    <li><a href="beginner.php"><img src="img/common/navi02.png" alt="初めての方へ" /></a></li>
    <li><a href="menu.php"><img src="img/common/navi03.png" alt="料金" /></a></li>
    <li><a href="access.php"><img src="img/common/navi04.png" alt="診察時間・アクセス" /></a></li>
    <li><a href="director.php"><img src="img/common/navi05.png" alt="院長紹介" /></a></li>
    <li><a href="inquiry.php"><img src="img/common/navi06.png" alt="お問い合わせ" /></a></li>
    <li><a href="http://ameblo.jp/yuki-shinkyu/" target="_blank"><img src="img/common/navi07.png" alt="ブログ" /></a></li>
	<div style="clear:both"></div>
</ul>
  

<div id="container">
<div id="box_l">
<p class="sp-mb50"><a href="beginner.php"><img src="img/common/forbeginner.png" alt="はじめての方へ" width="210" height="210" /></a></p>
<h3><img src="img/common/smenu.png" alt="メニュー" /></h3>
<ul id="smenu">
<li><a href="menu1.php"><img src="img/common/sindiva.png" alt="INDIVA" /></a></li>
<li><a href="menu2.php"><img src="img/common/shari.png" alt="鍼灸" /></a></li>
<li><a href="menu3.php"><img src="img/common/sbiyo.png" alt="美容鍼灸" /></a></li>
<li><a href="menu4.php"><img src="img/common/sseitai.png" alt="美容整骨" /></a></li>
</ul>
<p class="sp-mt40"><img src="img/common/credit.png" alt="クレジットカード使えます" /></a></p>
<p class="sp-mt20"><a href="http://ameblo.jp/yuki-shinkyu/"><img src="img/common/sblog.png" alt="ブログ" /></a></p>
<p class="sp-mt20"><a href="director.php"><img src="img/common/sintyou.png" alt="院長紹介" /></a></p>

<h4 class="sp-mt40"><img src="img/common/slogo.png" alt="ゆきしんきゅういん" /></h4>
<div class="slogo">
<p>兵庫県西宮市甲子園口北町4-29</p>
<p>マンション三和102号</p>
<p>［ TEL ]　0798-66-8834</p>
</div>
<p><a href="inquiry.php"><img src="img/common/smail.png" alt="ご予約・お問い合わせ" /></a></p>
<p style="margin-top:15px;"><a class="open_help_link" id="open_help_header2" href="javascript:void(0)"><img src="img/common/highquality01.jpg" alt="24時間web予約" width="200" height="111" /></a></p>

<dl class="LinksSide">
	<dt><img src="img/common/link_ttl.png" alt="リンク" /></dt>
    <dd>
    	<ul>
        	<li><a href="http://www.yuki-shinkyu.com/cocolo/index.php" target="_blank"><img src="img/common/link_bnr01.png" alt="cocolo"/></a></li>
    	</ul>
    </dd>
</dl>

</div>

<div id="box_r">
<h3><img src="img/biginner/beginner.png" alt="ようこそ！ゆきしんきゅういんへ" /></h3>

<div id="bgi_box1">
<div class="bgi1_l">
<div class="bgi_ttl">
<h4>アットホームな鍼灸院</h4>
</div>
</div>
<div class="bgi1_r">
<p>当院のホームページを閲覧いただき<br />
ありがとうございます。当院では、<br />
みなさんの症状や体質に合う治療を行い、<br />
生活を改善するアドバイスをします。<br />
西宮の地域のみなさまを中心に、健康の<br />
お手伝いができるよう日々努めてまいり<br />
ますので、末永くよろしくお願いいたします。<p>

  <p class="sp-mt30"><a href="blog.php"><img src="img/biginner/pro.png" alt="院長プロフィールはこちら" /></a></p>
  
  </div>





</div>
<div class="clear"></div>
<div id="bgi2">
<h3><img src="img/biginner/sinryou.png" alt="診療内容について" /></h3>
<p>当院では鍼灸治療と最新治療インディバをあわせた治療でお客様の悩みを解決させていただきます。</p>
<ul id="inl2">
<li><a href="menu1.php"><img src="img/biginner/indiva.png" width="279" height="90" /></a></li>
<li><a href="menu2.php"><img src="img/biginner/sinkyuu.png" /></a></li><div class="cle"></div>
</ul>
</div>


<h3 class="sp-mt40"><img src="img/biginner/price.png" alt="料金について" /></h3>
<p class="sp-mb30"><img src="img/biginner/osiharai.png" alt="お支払い方法" width="770" height="43" /></p>

<div class="bri3">
<div class="bri3_l">
<p>健康保険等については慢性疾患の方であれば医師の同意書があれば
適応することができます。詳しくはお問い合わせ下さい。</p>
<p>TEL：0798-66-8834 　|<a href="inquiry.php"><img src="img/biginner/smail.png" width="210" height="20" /></a></p>
</div>
<div class="bgi1_r">
<p class="sp-ml40"><a href="menu.php"><img src="img/biginner/sprice.png" alt="料金＆メニュー" /></a></p>
</div>
<div class="cle"></div>
</div>


<div class="bri3">
<h3 class="sp-mt40"><img src="img/biginner/user.png" alt="ご用意いただくもの" /></h3>
<div class="user1">
<p>上は肩・腕　下は膝の辺りがでるものであれば構いません。</p>
<p>着替えはご用意していますので仕事帰りの方でもそのままの服装でご来院下さい。</p>
</div>
<div class="user2">
<p>慢性疾患の方は医者の同意書があれば保険が適応されますので</p>
<p>持ち下さい。</p>
</div>
</div>

<div class="bri3">
<h3 class="sp-mt40"><img src="img/biginner/consu.png" alt="診療時間について" /></h3>

<table width="770" border="1" id="consu_tab">
  <tr bgcolor="#E8E2D1">
    <td width="220" height="50">受付時間　＼　曜日</td>
    <td width="80">月</td>
    <td width="80">火</td>
    <td width="80">水</td>
    <td width="80">木</td>
    <td width="80">金</td>
    <td width="80">土</td>
    <td width="80">日</td>
    <td width="80">祝</td>
  </tr>
  <tr>
    <td height="50">9　−　13時</td>
    <td>〇</td>
    <td>〇</td>
    <td>〇</td>
    <td>-</td>
    <td>〇</td>
    <td>〇</td>
    <td>〇</td>
    <td>△</td>
  </tr>
  <tr>
    <td height="50">14 　−　16時</td>
    <td>〇</td>
    <td>〇</td>
    <td>〇</td>
    <td>-</td>
    <td>〇</td>
    <td>〇</td>
    <td>△</td>
    <td>△</td>
  </tr>
  <tr>
    <td height="50">17　−　20時</td>
    <td>〇</td>
    <td>〇</td>
    <td>〇</td>
    <td>-</td>
    <td>〇</td>
    <td>-</td>
    <td>-</td>
    <td>-</td>
  </tr>
</table>

<p class="sp-mt20">予約受付　9：00〜20：00  　　　　　　 　◆休診日：<span style="color:#F00">木曜日</span>　※祝日は休診は事もありますので前もってご連絡ください</p>
</div>


<h3 class="sp-mt40"><img src="img/biginner/access.png" alt="アクセス" /></h3>
<div class="accesse1">
  ｜　JR甲子園口駅から徒歩2分 </div>

<div class="accesse2">
｜　駐車場が1台あります。<span class="syousai_f"> 　<a href="access.php">詳細はこちら。 </a>　 </span></div>


<p class="sp-mt40">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1639.3207472031915!2d135.37654510000001!3d34.739429999999984!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000f1f8e4b17c0d%3A0x4306e25300271ff3!2z5YW15bqr55yM6KW_5a6u5biC55Sy5a2Q5ZyS5Y-j5YyX55S677yU4oiS77yS77yZ!5e0!3m2!1sja!2sjp!4v1418307959064" width="770" height="260" frameborder="0" style="border:0"></iframe>

</p>

<!--ここからインフレ--->
<div class="sp-mt50">
<p class="t-c"><img src="img/biginner/ftlo.png" width="191" height="41" /></p></div>
<ul id="ft1" class="sp-pb20">
    <li><a href="menu1.php"><img src="img/biginner/ft1.png" /></a></li>
    <li><a href="menu2.php"><img src="img/biginner/ft2.png" /></a></li>
    <li><a href="menu3.php"><img src="img/biginner/ft3.png" /></a></li>
</ul>
<ul id="ft1" style="padding:50px 0 0 0;">
    <li><a href="menu4.php"><img src="img/biginner/ft4.png" /></a></li>
</ul>
<div class="cle"></div>
<div id="fts_box">
<ul id="fts">
<li><a href="beginner.php"><img src="img/biginner/fts1.png" /></a></li>
<li><a href="menu.php"><img src="img/biginner/fts2.png" /></a></li>
<li><a href="access.php"><img src="img/biginner/fts3.png" /></a></li>
</ul>
</div>
<div class="cle"></div>

<div id="ft_end">
<div id="ft_end_l">
<h4 class="sp-pl-40"><img src="img/biginner/ff1.png" alt="駐車場のご案内" /></h4>
<div class="sp-pl_t30">
<p>駐車スペースの台数に限りがありますので、<br />
お車でお越しの方はお電話にてご確認下さい。</p>
</div>
</div>


<div id="ft_end_r">
<h4><img src="img/biginner/ff2.png" alt="ご予約・お問い合わせ" /></h4>
<div class="sp-mt20">
<img src="img/biginner/tel.png" alt="TEL0798-66-8834" />
 　　　<a href="inquiry.php"><img src="img/biginner/ftmail.png" width="100" height="28" /></a></div>
</div>
<div class="cle"></div>
</div>

</div><!-- box_r -->




</div><!-- container -->



<?php include("footer.html"); ?>

</body>
</html>
